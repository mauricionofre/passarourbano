import { Component, OnInit } from '@angular/core';
import { OfertaService } from '../ofertas.service';
import { Oferta } from '../shared/oferta.model';

@Component({
  selector: 'app-restaurantes',
  templateUrl: './restaurantes.component.html',
  styleUrls: ['./restaurantes.component.scss'],
  providers: [OfertaService]
})
export class RestaurantesComponent implements OnInit {

  ofertas: Oferta[];

  constructor(
    private ofertaService: OfertaService
  ) { }

  ngOnInit() {
    this.ofertaService.getOfertasPorCategoria('restaurante')
      .then((resp: Oferta[]) => {
        this.ofertas = resp
      })
      .catch((param: any) => console.log(param))
  }

}
