import { Component, OnInit } from '@angular/core';
import { OfertaService } from '../ofertas.service';
import { Oferta } from '../shared/oferta.model';

@Component({
  selector: 'app-diversao',
  templateUrl: './diversao.component.html',
  styleUrls: ['./diversao.component.scss'],
  providers: [OfertaService]
})
export class DiversaoComponent implements OnInit {

  ofertas: Oferta[];

  constructor(
    private ofertaService: OfertaService
  ) { }

  ngOnInit() {
    this.ofertaService.getOfertasPorCategoria('diversao')
      .then((resp: Oferta[]) => {
        this.ofertas = resp
      })
      .catch((param: any) => console.log(param))
  }

}
