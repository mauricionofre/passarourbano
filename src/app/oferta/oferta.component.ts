import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Oferta } from '../shared/oferta.model';
import { OfertaService } from '../ofertas.service';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.scss'],
  providers: [OfertaService]
})
export class OfertaComponent implements OnInit {

  oferta: Oferta;

  constructor(
    private route: ActivatedRoute,
    private service: OfertaService,
  ) { }

  ngOnInit() {
    this.service.getOfertaPorId(this.route.snapshot.params['id'])
    .then((resp: Oferta) => {
      this.oferta = resp
    })
  }

}
