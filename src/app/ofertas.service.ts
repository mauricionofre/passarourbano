import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Oferta } from './shared/oferta.model';
import { environment } from '../environments/environment';

@Injectable()
export class OfertaService {

    private readonly URLAPI = `${environment.urlApi}/ofertas`
    
    constructor(
        private http: HttpClient,
    ){}

    public getOfertas(): Promise<Oferta[]> {
        return this.http.get(`${this.URLAPI}?destaque=true`)
            .toPromise()
            .then((resp:any) => resp);
    }

    getOfertasPorCategoria(categoria: string): Promise<Oferta[]> {
        return this.http.get(`${this.URLAPI}?categoria=${categoria}`)
        .toPromise()
        .then((resp: any) => resp)
    }

    getOfertaPorId(id: number): Promise<Oferta>{
        return this.http.get(`${this.URLAPI}/${id}`)
        .toPromise()
        .then((resp: Oferta) => resp);
    }
}